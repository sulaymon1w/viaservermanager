package uz.ssd.server.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.ssd.server.models.Response;
import uz.ssd.server.service.FirewallService;

@RestController
@RequestMapping("/api/firewall")
public class FirewallController {

    final FirewallService service;

    public FirewallController(FirewallService service) {
        this.service = service;
    }

    @GetMapping
    public Response getAll() {
        return null;
    }
}
