package uz.ssd.server.controller;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.ssd.server.models.ErrorsField;
import uz.ssd.server.models.Response;
import uz.ssd.server.models.Status;
import uz.ssd.server.payload.LogModel;
import uz.ssd.server.service.LogService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/logger")
public class LogController {

    final LogService service;

    public LogController(LogService service) {
        this.service = service;
    }

    @PatchMapping
    public Response connectServer(@Valid @RequestBody LogModel model, BindingResult error, HttpServletRequest request) {
        return !error.hasErrors() ? service.connectServer(model, request.getRemoteAddr()) : new Response(new Status(400, "Bed request"), error.getFieldErrors().stream().map(fieldError -> new ErrorsField(fieldError.getField(), fieldError.getDefaultMessage())).collect(Collectors.toList()), null);
    }
}
