/*
package uz.ssd.server.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class LeetCode {
    public static void main(String[] args) {
        System.out.println(sumOfUnique(new int[]{1, 1, 1, 1, 1}));
    }

    private static List<Integer> list;
    private static List<Integer> dub = new ArrayList<>();

    public static int sumOfUnique(int[] nums) {
        list = new ArrayList<>();
        int i = sumOfUniqueMini(Arrays.stream(nums).boxed().collect(Collectors.toList()));
        return i;
    }//https://leetcode.com/problems/sum-of-unique-elements/

    public static int sumOfUniqueMini(List<Integer> nums) {
        if (!nums.isEmpty()) {
            List<Integer> list = nums.subList(1, nums.size());
            if (list.contains(nums.get(0))) {
                dub.add(nums.get(0));
            } 
            sumOfUniqueMini(list);
        }
        return list.stream().reduce(0, Integer::sum);
    }
}
*/
