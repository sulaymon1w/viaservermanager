package uz.ssd.server.service;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;
import uz.ssd.server.models.ErrorsField;
import uz.ssd.server.models.Response;
import uz.ssd.server.models.Status;
import uz.ssd.server.payload.LogModel;
import uz.ssd.server.util.Logger;

import javax.validation.Valid;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static uz.ssd.server.util.Connector.listFolderStructure;
import static uz.ssd.server.util.Connector.setSession;

@Service
public class LogService {
    private final Logger logger = new Logger(LogService.class);
    private final Map<String, BigInteger> map = new HashMap<>();

    public Response connectServer(@Valid LogModel model, String ip4v) {
        try {
            String key = model.getIp() + model.getPassword() + ip4v + model.getLogPath();
            if (!setSession(model.getUser(), model.getPassword(), model.getIp(), model.getPort())) {
                return new Response(new Status(500), List.of(new ErrorsField("ssh", "cannot connect")), null);
            }
            BigInteger showing = map.getOrDefault(key, new BigInteger("0"));
            String log = listFolderStructure(model.getUser(), model.getPassword(), model.getIp(), model.getPort(), "awk 'NR >=$number' $path".replace("$number", showing.toString()).replace("$path", model.getLogPath()));
            String[] split = log.split("\n");
            map.put(key, log.split("\n").length > 1 ? new BigInteger(String.valueOf(split.length)) : showing);
            return new Response(new Status(200), List.of(), split.length > 1 ? split : List.of());
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new Response(new Status(500), List.of(new ErrorsField("error", e.getMessage())), List.of());
        }
    }


    @ExceptionHandler(value = Exception.class)
    public Response handleException(Exception e) {
        logger.error(e.getMessage());
        return new Response();
    }
}
