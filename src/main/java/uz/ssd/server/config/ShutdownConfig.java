package uz.ssd.server.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import uz.ssd.server.util.Logger;

import javax.annotation.PreDestroy;

@Configuration
public class ShutdownConfig {

    private final Logger logger = new Logger(ShutdownConfig.class);

    @Bean
    public TerminateBean getTerminateBean() {
        return new TerminateBean();
    }

    class TerminateBean {
        @PreDestroy
        public void onDestroy() throws Exception {
            logger.info("project down bro :((");
            logger.info(new String(Runtime.getRuntime().exec("curl -X POST https://textbelt.com/text --data-urlencode phone='+998909476154' --data-urlencode message='Assalamu-alaykum-Xojiaka-project-o`chib-qoldiyov:))' -d key=textbelt").getInputStream().readAllBytes()));
        }
    }
}
