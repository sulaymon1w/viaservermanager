package uz.ssd.server.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LogModel {
    @NotNull
    private String ip;
    @NotNull
    private String user;
    @NotNull
    private String password;
    @NotNull
    @Min(value=18)
    private int port;
    @NotNull
    private String logPath;
}
