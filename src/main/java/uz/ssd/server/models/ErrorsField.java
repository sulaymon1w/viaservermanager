package uz.ssd.server.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorsField {
    private String field;
    private String expelling;
}
