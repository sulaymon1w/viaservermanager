package uz.ssd.server.util;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

@Service
public class Connector {
    static Logger logger = new Logger(Connector.class);

    private static final Map<String, Session> session = new HashMap<>();

    public static boolean setSession(String username, String password, String host, int port) {
        try {
            if (session.get(username + password + host) == null) {
                Session sessionInfo = null;
                sessionInfo = new JSch().getSession(username, host, port);
                sessionInfo.setPassword(password);
                sessionInfo.setConfig("StrictHostKeyChecking", "no");
                sessionInfo.connect();
                session.put(username + password + host, sessionInfo);
            }
            return true;
        } catch (Exception e) {
            logger.error(e);
            return false;
        }
    }

    public static String listFolderStructure(String username, String password, String host, int port, String command) {
        try {
            Session sessionInfo = session.get(username + password + host);
            ChannelExec channel = null;
            try {
                channel = (ChannelExec) sessionInfo.openChannel("exec");
                channel.setCommand(command);
                ByteArrayOutputStream responseStream = new ByteArrayOutputStream();
                channel.setOutputStream(responseStream);
                channel.connect();

                while (channel.isConnected()) {
                    Thread.sleep(100);
                }

                return responseStream.toString();
            } finally {
                if (channel != null) {
                    channel.disconnect();
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            return e.getMessage();
        }
    }

    @Scheduled(cron = "* * */23 * * *")
    private void clear() {
        session.clear();
        logger.info("cache cleared");
    }
}
